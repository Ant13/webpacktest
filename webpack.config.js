const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetWebPackPlugin = require('optimize-css-assets-webpack-plugin');
const TerserWebPackPkugin = require('terser-webpack-plugin');


const isDev = process.env.NODE_ENV === 'development';
const isProd = !isDev;

const filename = ext => isDev ? `[name].${ext}` : `[name].[hash].${ext}`;

const bableOptions = (preset) => {
    const options = {
        presets: [
            '@babel/preset-env',
        ]
    };

    if(preset){
        options.presets.push(preset);
    }

    return options;
}
const optimization = () => {
    const config = {
        splitChunks:{
            chunks: 'all'
        }
    };

    if(isProd){
        config.minimizer = [
            new OptimizeCssAssetWebPackPlugin(),
            new TerserWebPackPkugin(),
        ]
    }

    return config;
};

module.exports={
    context: path.resolve(__dirname, 'src'),
    entry: {
        main: ['@babel/polyfill','./index.js'],
        //other js files
    },
    output:{
        filename: filename("js"), 
        path: path.resolve(__dirname, 'public')
    },
    resolve: {
        extensions: ['.js', '.json', '.svg'],
        alias:{
            
        }
        
    },
    optimization: optimization(),
    devServer:{
        port: 3200,
        //hot: isDev
    },
    devtool: isDev ? 'source-map' : '',
    plugins:[
        new HtmlWebpackPlugin({
            template: './index.html',
            minify:{
                collapseWhitespace: isProd
            }
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename:  filename('css'),
        })
    ],
    module:{
        rules: [
            {
                test: /\.s[ac]ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: isDev,
                            reloadAll: true,
                        },
                    },
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|gif|jpg|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'assets/images/'
                        }
                    }
                ]
            },
            {
                test: /\.(ttf|woff|woff2|eot)$/,
                use: ['file-loader']
            },
            {
                test: /\.xml$/,
                use: ['xml-loader']
            },
            {
                test: /\.csv$/,
                use: ['csv-loader']
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: {
                    loader: 'babel-loader',
                    options: bableOptions()
                }
            },
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: {
                    loader: 'babel-loader',
                    options: bableOptions('@babel/preset-typescript')
                }
            },
            {
                test: /\.html$/i,
                loader: 'html-loader',
            },
        ]
    }
}