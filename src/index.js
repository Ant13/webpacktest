import {
    Swiper,
    Navigation,
    Pagination,
    Scrollbar,
    EffectCoverflow
} from 'swiper';
import './styles/index.scss';
import './index.html';

Swiper.use([Navigation, Pagination, Scrollbar, EffectCoverflow]);


const getNamesPagination = () => {
    const element = document.querySelectorAll('.swiper-slide-m');
    const titles = Object.keys(element).map((index) => {
        return element[index].getAttribute('data-title')
    });
    return titles;
};

const menu = getNamesPagination();

const swiperImage1 = new Swiper('.swiper-container-images-1', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    navigation:{
        nextEl: '.swiper-button-next-i-1',
        prevEl: '.swiper-button-prev-i-1',
    },
});

const swiperImage2 = new Swiper('.swiper-container-images-2', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    navigation:{
        nextEl: '.swiper-button-next-i-2',
        prevEl: '.swiper-button-prev-i-2',
    },
});

const swiperImage3 = new Swiper('.swiper-container-images-3', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    navigation:{
        nextEl: '.swiper-button-next-i-3',
        prevEl: '.swiper-button-prev-i-3',
    },
});

const swiper = new Swiper('.swiper-container-main', {
    navigation:{
        nextEl: '.swiper-button-next-m',
        prevEl: '.swiper-button-prev-m',
    },
    pagination:{
        el: '.swiper-pagination.pagination-bottom, .swiper-pagination.pagination-top',
        clickable: true,
        renderBullet: (index, className) => {
            return '<span class="' + className + '">' + menu[index] +  '</span>';
        },
    },
});
